import * as React from 'react';

import './Button.css';

export default function Button({children, ...rest}) {
    return (
        <button {...rest} className="asi-button">{children}</button>
    );
}
