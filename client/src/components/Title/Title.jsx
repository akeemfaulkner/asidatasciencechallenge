import * as React from 'react';

import './Title.css';

export default function Title({children, ...rest}) {
    return (
        <h4 className="asi-title" {...rest}>{children}</h4>
    );
}
