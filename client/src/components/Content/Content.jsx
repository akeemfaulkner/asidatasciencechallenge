import * as React from 'react';

import './Content.css';

export default function Content({children}) {
    return (
        <div className="asi-content">{children}</div>
    );
}
