import * as React from 'react';

import './Circle.css';

export default function Circle({color}) {
    return (
        <span className="asi-circle" style={{background: color}}/>
    );
}
