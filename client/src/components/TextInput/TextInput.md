TextInput example:

    <div>
        <TextInput placeholder="Type a valid Github username"/>
        <TextInput placeholder="Type a valid Github username" hasError={true}/>
    </div>