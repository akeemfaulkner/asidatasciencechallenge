import * as React from 'react';

import './TextInput.css';

export default class TextInput extends React.Component {

    constructor(props) {
        super(props);
        this.inputRef = null;
    }

    render() {
        const {hasError, ...rest} = this.props;
        return (
            <input ref={ref => this.inputRef = ref} type="text" {...rest}
                   className={`asi-input ${hasError ? 'asi-input--has-error' : ''}`}/>
        );
    }

    clear() {
        this.inputRef.value = '';
    }
}