import * as React from 'react';

import './Panel.css';

export default function Panel({children}) {
    return (
        <div className="asi-panel">{children}</div>
    );
}
