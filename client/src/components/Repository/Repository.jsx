import * as React from 'react';

import './Repository.css';
import Panel from "../Panel/Panel";
import Title from "../Title/Title";
import Content from "../Content/Content";
import Circle from "../Circle/Circle";
import Star from "../Star/Star";
import Share from "../Share/Share";

import languagesConfig from '../../shared/languages.config';

export default function Repository({title, content, language, stars, forks, url}) {

    const languageConfig = languagesConfig[language];

    return (
        <div className="asi-repository">
            <Panel>
                <Title onClick={() => window.open(url)}>{title}</Title>
                <Content>{content}</Content>

                <div className="asi-repository__icon-list">
                    <div className="asi-repository__icon">
                        {languageConfig ? <Circle color={languageConfig.color}/> : null }{language}
                    </div>
                    <div className="asi-repository__icon">
                        <Star/> {stars}
                    </div>
                    <div className="asi-repository__icon">
                        <Share/> {forks}
                    </div>
                </div>
            </Panel>
        </div>
    );
}
