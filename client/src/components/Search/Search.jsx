import React, {Component} from "react";
import TextInput from "../TextInput/TextInput";
import Button from "../Button/Button";

import './Search.css';

export default class Search extends Component {

    constructor(props) {
        super(props);
        this.state = {
            query: ''
        };

        this.handleInput = this.handleInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        /** @type {TextInput} */
        this.textRef = null;
    }

    render() {

        const {hasError} = this.props;

        return (
            <div className="asi-search">
                <TextInput
                    ref={ref => this.textRef = ref}
                    hasError={hasError}
                    onInput={this.handleInput}
                    placeholder="Type a valid Github username"/>

                <Button onClick={this.handleSubmit}>Get Repositories</Button>
            </div>
        );
    }

    handleSubmit() {
        this.props.onSubmit(this.state.query);
        this.setState({query: ''});
        this.textRef.clear();
    }

    handleInput(e) {
        this.setState({query: e.target.value});
    }
}