import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import RepositoryReducer from "./shared/actions/repository/reducer";
import repositoryMiddleware from "./shared/actions/repository/middleware";


let rootReducer = combineReducers({
    repository: new RepositoryReducer().reduce,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(...[repositoryMiddleware])
));

export default store;