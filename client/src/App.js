import React, {Component} from 'react';
import Search from "./components/Search/Search";

import './App.css';
import Repository from "./components/Repository/Repository";
import {connect} from "react-redux";
import * as repositoryActions from "./shared/actions/repository/ActionCreator";
import {bindActionCreators} from "redux";

class App extends Component {

    constructor(props) {
        super(props);
        this.handleSearch = this.handleSearch.bind(this);
    }

    render() {

        const {repository} = this.props;

        return (
            <div className="wrapper">
                <div className="asi-header">
                    <Search onSubmit={this.handleSearch} hasError={repository.hasError}/>
                </div>

                <div className="asi-repositories">

                    {repository.results.map(repository =>
                        <Repository {...repository}/>)}

                </div>
            </div>
        );
    }

    handleSearch(q) {
        const {actions: {search, clear}} = this.props;
        search(q);
        clear();
    }
}

export default connect(
    mapState,
    mapDispatch
)(App);

function mapState(state) {
    return state;
}


function mapDispatch(dispatch) {
    return {
        actions: bindActionCreators(repositoryActions, dispatch)
    }
}