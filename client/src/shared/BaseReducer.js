export default class BaseReducer {

    constructor(getDefaultState) {
        this.defaultState = getDefaultState();
        this.resetState = ()=>getDefaultState();
        this.handlers = {};
        this.reduce = this.reduce.bind(this);
    }


    reduce(state, action) {
        let handler = this.handlers[action.type];
        let currentState = state || this.defaultState;
        return handler ? handler(currentState, action) : currentState;
    }
}