import BaseReducer from '../../BaseReducer';
import EventType from "./EvenType";


export default class Reducer extends BaseReducer {

    constructor() {
        super(getDefaultState);

        this.handlers = {
            [EventType.RESULT]: this.handleResult.bind(this),
            [EventType.ERROR]: this.handleError.bind(this),
            [EventType.CLEAR]: this.resetState.bind(this),
        };
    }

    handleResult(state, {payload}) {
        return getDefaultState(payload, false);
    }

    handleError() {
        return getDefaultState([], true);
    }

}

export function getDefaultState(results = [], hasError = false) {
    return {
        results,
        hasError,
        config: {
            url: 'http://localhost:3000/api'
        }
    };
}
