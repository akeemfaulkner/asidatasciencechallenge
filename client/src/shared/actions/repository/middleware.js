import EventType from "./EvenType";
import {error, result} from "./ActionCreator";
require('es6-promise').polyfill();
require('isomorphic-fetch');

const repositoryMiddleware = store => next => async action => {
    if (action && action.type === EventType.SEARCH) {

        const apiUrl = store.getState().repository.config.url;

        try {

            const response = await fetch(`${apiUrl}/${action.payload}`, {method: 'get'})
                .then(function (response) {
                    if (response.status >= 400)  return store.dispatch(error());
                    return response.json();
                });

            if (response.error)  return store.dispatch(error());

            store.dispatch(result(response));

        } catch (e) {
            return store.dispatch(error());
        }


    }
    return next(action)
};

export default repositoryMiddleware;