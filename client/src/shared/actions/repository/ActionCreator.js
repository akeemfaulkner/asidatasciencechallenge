import {createAction} from '../../utils';
import EventType from "./EvenType";

export const error = createAction(EventType.ERROR);
export const result = createAction(EventType.RESULT);
export const search = createAction(EventType.SEARCH);
export const clear = createAction(EventType.CLEAR);
