const EventType = {
    RESULT : 'shared.actions.cart.search/success',
    ERROR : 'shared.actions.cart.search/error',
    SEARCH : 'shared.actions.cart.search',
    CLEAR : 'shared.actions.cart.clear',
};

export default EventType;