# Questions

Q. How long did you spend on the coding test? 
A. 2h 40m

Q. What would you add to your solution if you had more time? 
A. 
    - create a proper build script
    - I would have liked to break down the unit test in to more focused sections.
    - Write actual Frontend tests to see how state changes the UI  
    - test the reducers to check that the state is updated as expected
    - Add prop-types to components and containers to enforce typing during development
    - add more UI feedback for error handling
    - restrict origins, authenticate calls and add a limit on calls to prevent attacks

Q. If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add.

A. Please describe yourself using JSON.
    
Q.    
   ```json
      {
        "name" : {
          "first" : "Akeem",
          "last" : "Faulkner"
        },
        "attributes": [
          "Creative",
          "Pragmatic",
          "Confident",
          "Positive",
          "Honest",
          "Respectful",
          "Communicator"
        ]
      }
   ```

Q. How would you improve the application's performance?

A. I would have added a short cache for responses from the github api based upon username