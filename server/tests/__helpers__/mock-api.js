const nock = require('nock');

const base_url = 'https://api.github.com';

const errorMock = nock(base_url)
    .get('/users/error/repos')
    .reply(404, []);

const serverMock = nock(base_url)
    .get('/users/server/repos')
    .reply(500, []);

const successMock = nock(base_url)
    .get('/users/success/repos')
    .reply(200, require('./mock-response.json'));

module.exports = {
    errorMock,
    serverMock,
    successMock
};