const {
    successMock
} = require('./__helpers__/mock-api');

const expect = require('expect.js');
const nock = require('nock');
const request = require('supertest');
const express = require('express');
const app = require('../app');

describe('ASIDataScienceChallenge App', () => {

    afterEach(() => {
        if (successMock.isDone()) {
            nock.cleanAll();
        }
    });

    describe('No username found', () => {
        it('should return 400 if the username cannot be found', (done) => {
            request(app)
                .get('/api/error')
                .expect(400)
                .end(function (err, res) {
                    if (err) throw err;
                    expect(res.body.error).to.equal('User could not be found');
                    done();
                });
        });
    });


    describe('Server error', () => {
        it('should return 500 if there is a service error', (done) => {
            request(app)
                .get('/api/server')
                .expect(500)
                .end(function (err, res) {
                    if (err) throw err;
                    expect(res.body.error).to.equal('Whoops! An error occurred');
                    done();
                });
        });
    });

    describe('Repository response', () => {
        it('should return 200 and top 10 repositories', (done) => {
            expect(require('./__helpers__/mock-response.json').length > 10).to.equal(true);
            request(app)
                .get('/api/success')
                .expect(200)
                .end(function (err, res) {
                    expect(res.body.length <= 10).to.equal(true);
                    if (err) throw err;
                    done();
                });
        });

        it('should return 200 and repositories ordered by stars descending', (done) => {
            request(app)
                .get('/api/success')
                .expect(200)
                .end(function (err, res) {
                    res.body.forEach(function (repo, index, repos) {
                        if (index > 0) {
                            expect(repo.stargazers_count <= repos[index - 1].stargazers_count)
                                .to.equal(true);
                        }
                    });
                    if (err) throw err;
                    done();
                });
        });
    });


});