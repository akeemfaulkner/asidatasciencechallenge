const express = require('express');
const router = express.Router();
const GitApiController = require('../controllers/GitApiController');

router.get('/:username', GitApiController.get);

module.exports = router;
