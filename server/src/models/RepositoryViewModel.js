class RepositoryViewModel {
    constructor({name, description, language, stargazers_count, forks_count, html_url}) {
        this.title = name;
        this.content = description;
        this.language = language;
        this.stars = this.formatNumber(stargazers_count, 1);
        this.stargazers_count = stargazers_count;
        this.forks = this.formatNumber(forks_count, 1);
        this.forks_count = forks_count;
        this.url = html_url;
    }

    formatNumber(num, digits) {
        const si = [
            {value: 1E18, symbol: "E"},
            {value: 1E15, symbol: "P"},
            {value: 1E12, symbol: "T"},
            {value: 1E9, symbol: "G"},
            {value: 1E6, symbol: "M"},
            {value: 1E3, symbol: "k"}
        ], rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
        for (let i = 0; i < si.length; i++) {
            if (num >= si[i].value) {
                return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
            }
        }
        return num.toFixed(digits).replace(rx, "$1");
    }
}

module.exports = RepositoryViewModel;