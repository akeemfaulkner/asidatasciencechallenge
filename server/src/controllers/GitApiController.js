require('es6-promise').polyfill();
require('isomorphic-fetch');

const RepositoryViewModel = require('../models/RepositoryViewModel');

class GitApiController {

    constructor() {
        this.httpConfig = {
            method: 'GET',
            headers: {
                Accept: 'application/vnd.github.v3+json',
            },
            mode: 'cors',
            cache: 'default',
        };

        this.get = this.get.bind(this);
    }

    get(request, response) {
        const {username} = request.params;
        fetch(`https://api.github.com/users/${username}/repos`, this.httpConfig)
            .then(res => {
               
                res.json().then(body => {
                    if (res.status === 200) {

                        const sortedReposByStars = body.sort((a, b) => b.stargazers_count - a.stargazers_count);
                        const top10Repos = sortedReposByStars.slice(0, 10);

                        response.status(200).json(top10Repos.map(repository => new RepositoryViewModel(repository)));
                    } else if (res.status === 404) {
                        response.status(400).json({error: 'User could not be found'});
                    } else {
                        response.status(500).json({error: 'Whoops! An error occurred'});
                    }
                    response.end();
                });
            });
    };
}

module.exports = new GitApiController();