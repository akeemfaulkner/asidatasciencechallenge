# ASIDataScienceChallenge

## Table of Contents

- [Running](#running)
- [Tests](#tests)

## Running

From the root of the project run the following commands to install, build and run the application
```bash
# using npm
npm start

# or using yarn
yarn start
```

## Tests

Due to time only tests for the backend have been written.
 They can be run from the [server](/server) directory, running the following command after dependencies have been installed.
```bash
# using npm
npm test

# or using yarn
yarn test
```

When the application starts it will run on localhost:3000
